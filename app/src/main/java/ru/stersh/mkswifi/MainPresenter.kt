package ru.stersh.mkswifi

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import moe.codeest.rxsocketclient.RxSocketClient
import moe.codeest.rxsocketclient.SocketClient
import moe.codeest.rxsocketclient.SocketSubscriber
import moe.codeest.rxsocketclient.meta.SocketConfig
import moe.codeest.rxsocketclient.meta.ThreadStrategy.ASYNC
import moxy.MvpPresenter
import java.nio.charset.StandardCharsets.UTF_8


class MainPresenter : MvpPresenter<MainView>() {
    private val lifecycle = CompositeDisposable()
    private var socket: SocketClient? = null

    override fun onFirstViewAttach() {
        viewState.setDisconnected()
    }

    fun disconnect() {
        socket?.disconnect()
    }

    fun send(string: String) {
        val cmd = string + "\n"
        viewState.printMessage("\n>>>>$cmd\n")
        socket?.sendData(string + "\n")
    }

    fun connect(address: String) {
        socket = RxSocketClient.create(
            SocketConfig.Builder()
                .setIp(address)
                .setPort(8080)
                .setCharset(UTF_8)
                .setThreadStrategy(ASYNC)
                .setTimeout(30 * 1000)
                .build()
        )
        socket
            ?.connect()
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe(object : SocketSubscriber() {
                override fun onConnected() {
                    viewState.setConnected()
                }

                override fun onDisconnected() {
                    viewState.setDisconnected()
                }

                override fun onResponse(data: ByteArray) {
                    viewState.printMessage(data.toString(UTF_8))
                }
            })
            ?.addTo(lifecycle)
    }

    override fun onDestroy() {
        super.onDestroy()
        lifecycle.dispose()
    }
}