package ru.stersh.mkswifi

import moxy.MvpView
import moxy.viewstate.strategy.alias.AddToEnd
import moxy.viewstate.strategy.alias.OneExecution
import moxy.viewstate.strategy.alias.SingleState

interface MainView : MvpView {
    @AddToEnd
    fun printMessage(message: String)

    @SingleState
    fun setConnected()

    @SingleState
    fun setDisconnected()

    @OneExecution
    fun showError(string: String)
}