package ru.stersh.mkswifi

import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import moxy.MvpAppCompatActivity
import moxy.ktx.moxyPresenter

class MainActivity : MvpAppCompatActivity(), MainView {
    private val presenter by moxyPresenter { MainPresenter() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        scrollable.viewTreeObserver.addOnGlobalLayoutListener {
            scrollable.post {
                scrollable.fullScroll(View.FOCUS_DOWN);
            }
        }
    }

    override fun printMessage(message: String) {
        val prevText = terminal.text.toString()
        terminal.text = prevText + message
    }

    override fun setConnected() {
        address.isEnabled = false
        connection.text = "Отключиться"
        connection.setOnClickListener { presenter.disconnect() }

        command.isEnabled = true
        send.isEnabled = true
        send.setOnClickListener {
            val commandToSend = command.text.toString()
            if (commandToSend != null && commandToSend.isNotEmpty()) {
                presenter.send(commandToSend)
            }
        }
    }

    override fun setDisconnected() {
        address.isEnabled = true
        connection.text = "Подключиться"
        connection.setOnClickListener {
            val text = address.text
            if (text != null && text.isNotEmpty()) {
                presenter.connect(text.toString())
            }
        }

        command.isEnabled = false
        send.isEnabled = false
    }

    override fun showError(string: String) {
        Toast.makeText(this, string, Toast.LENGTH_SHORT).show()
    }
}
